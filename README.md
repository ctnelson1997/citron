# Citron

A purely functional programming language designed to address issues of side-effects and provide tuples as a way of communicating data.